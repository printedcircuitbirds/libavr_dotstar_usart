/*
 * dotstar_usart.h
 *
 * Created: 22.03.2020
 * Author: echoromeo
 */
#ifndef DOTSTAR_USART_H_
#define DOTSTAR_USART_H_

/*
 * Include the project config
 */
#include <dotstar_config.h>

#define DOTSTAR_LED_BYTES   (4)

/*
 * The data type used for configuring the LEDs
 */
typedef __uint24 uint24_t;

typedef union {
	uint8_t array[4];
	struct {
		uint8_t brightness : 5;
		uint8_t ones	   : 3; //Always set to 0x7
		union {
			struct {
				uint8_t b;
				uint8_t g;
				uint8_t r;
				};
				uint24_t channel;
			};
	};
} color_t;

/*
 * Private functions
 */

// static void dotstar_usart_send_byte(uint8_t byte);

/*
 * Public functions
 */

void dotstar_usart_init(uint8_t prescaling);

void dotstar_usart_write_start(void);
void dotstar_usart_write_end(uint16_t total_length);
void dotstar_usart_write_single(color_t color);
void dotstar_usart_write_array(color_t *array, uint16_t length);
void dotstar_usart_write_constant(color_t color, uint16_t length);

void dotstar_usart_configure_neopixel_rgb_array(uint24_t *array, uint16_t length, uint8_t brightness);


#endif /* DOTSTAR_USART_H_ */
