/*
 * dotstar_usart.c
 *
 * Created: 22.03.2020
 * Author: echoromeo
 */

#include <avr/io.h>
#include <stdint.h>
#include "dotstar_usart.h"

/*
 * Local defines
 */


/*
 * Private functions
 */
static void dotstar_usart_send_byte(uint8_t byte)
{
	while(!(LED_USART.STATUS & USART_DREIF_bm)) {
		// Wait for empty buffer
	}

	LED_USART.TXDATAL = byte;
}

/*
 * Public functions
 */
void dotstar_usart_init(uint8_t prescaling)
{
	LED_USART.CTRLB = USART_TXEN_bm;
	LED_USART.CTRLC = USART_CMODE_MSPI_gc;

	// In case you have a lot of LEDs in the string and running at high F_CPU
	if (!prescaling) {
		prescaling = 1;
	}
	LED_USART.BAUD = 64*prescaling; //64 is minimum

	// Update PORTMUX to alternate USART pinout, if used
	#if defined(PORTMUX_CTRLB) // Used by tiny 0- and 1-series
		PORTMUX.CTRLB |= LED_USART_PORT_ALT;  
	#elif defined(PORTMUX_USARTROUTEA) // Used by the rest of the later devices
		#if (defined(PORTMUX_USARTROUTEB) && defined(LED_USART_PORT_ALTB))
			// If using USART4 or USART5 on the large Dx devices
			PORTMUX.USARTROUTEB |= LED_USART_PORT_ALTB;
		#else 
			PORTMUX.USARTROUTEA |= LED_USART_PORT_ALT;
		#endif
	#endif

	// Set MOSI and SCK to output
	LED_USART_PORT.OUTCLR = LED_DATA_PIN | LED_SCK_PIN;
	LED_USART_PORT.DIRSET = LED_DATA_PIN | LED_SCK_PIN;
}

void dotstar_usart_write_start(void) {
	for(uint8_t Byte_Count = DOTSTAR_LED_BYTES; Byte_Count > 0; Byte_Count--) {
		dotstar_usart_send_byte(0x00);
	}	
}

void dotstar_usart_write_end(uint16_t total_length) {
	for(uint8_t Byte_Count = DOTSTAR_LED_BYTES+(total_length/16)+1; Byte_Count > 0; Byte_Count--) {
		dotstar_usart_send_byte(0x00);
	}

	while(!(LED_USART.STATUS & USART_TXCIF_bm));
	LED_USART.STATUS = USART_TXCIF_bm;
}

void dotstar_usart_write_single(color_t color)
{
	for(uint8_t Byte_Count = 0; Byte_Count < DOTSTAR_LED_BYTES; Byte_Count++) {
		dotstar_usart_send_byte(color.array[Byte_Count]);
	}
}

void dotstar_usart_write_array(color_t *array, uint16_t length)
{
	uint8_t *LED_Bytes = (uint8_t *) array;
	for(uint16_t Byte_Count = 0; Byte_Count < length*DOTSTAR_LED_BYTES; Byte_Count++) {
		dotstar_usart_send_byte(LED_Bytes[Byte_Count]);
	}
}

void dotstar_usart_write_constant(color_t color, uint16_t length)
{
	while(length--) {
		dotstar_usart_write_single(color);
	}
}

void dotstar_usart_configure_neopixel_rgb_array(uint24_t *array, uint16_t length, uint8_t brightness)
{
	brightness |= (0x7 << 5); // Top 3 bits always 1
	uint8_t *LED_Bytes = (uint8_t *) array;
	dotstar_usart_write_start();
	for(uint16_t count = 0; count < length; count++) {
		dotstar_usart_send_byte(brightness);
		dotstar_usart_send_byte(*LED_Bytes++);
		dotstar_usart_send_byte(*LED_Bytes++);
		dotstar_usart_send_byte(*LED_Bytes++);
	}
	dotstar_usart_write_end(length);
}
