/*
 * dotstar_example.c - short project to test the library and show it can be used
 *
 * Created: 22.03.2020
 * Author: echoromeo
 */
#include <avr/io.h>
#include <util/delay.h>
#include <dotstar.h> //Remember to add the library path to the compiler directories


// Just an array with 10 different colors
static color_t data[] = {
	dotstar_color(0xff, 0,    0,	0x0A),
	dotstar_color(0,    0xff, 0,	0x0A),
	dotstar_color(0,    0,    0xff,	0x0A),
	dotstar_color(0,    0x7f, 0x7f,	0x0A),
	dotstar_color(0x7f, 0,    0x7f,	0x0A),
	dotstar_color(0x7f, 0x7f, 0,	0x0A),
	dotstar_color(0x55, 0x55, 0x55,	0x0A),
	dotstar_color(0x80, 0x40, 0x40,	0x0A),
	dotstar_color(0x40, 0x80, 0x40,	0x0A),
	dotstar_color(0x40, 0x40, 0x80,	0x0A),
};

int main(void)
{
	// Configure clock
	#if ((F_CPU == 20000000ul) || (F_CPU == 16000000ul))
		_PROTECTED_WRITE(CLKCTRL.MCLKCTRLB, 0);
	#elif ((F_CPU == 10000000ul) || (F_CPU == 8000000ul))
		_PROTECTED_WRITE(CLKCTRL.MCLKCTRLB, CLKCTRL_PDIV_2X_gc | CLKCTRL_PEN_bm);
	#endif
	
	// Init the driver
	dotstar_init(1);

	// Send the array to 10 LEDs
	dotstar_configure_array(data, sizeof(data)/sizeof(data[0]));
	_delay_ms(3000);
	
	// Turn off all but one red LED that moves
	uint8_t loop = 0;
	color_t red = dotstar_color(0x7f, 0, 0, 0x10);
    while (1) 
    {

		dotstar_configure_off_and_single(red, loop++, sizeof(data)/sizeof(data[0]));

		if (loop >= sizeof(data)/sizeof(data[0]))
		{
			loop = 0;
		}

		_delay_ms(100);
    }
}
